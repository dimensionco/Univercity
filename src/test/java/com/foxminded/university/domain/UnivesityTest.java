package com.foxminded.university.domain;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnivesityTest {

    static Lesson lessonOne = new Lesson();
    static University university = new University();
    
    @BeforeClass
    public static void setUp() throws Exception {

        Auditorium auditorium = new Auditorium();
        auditorium.setNumber("4410");
        
        Course astron = new Course();
        astron.setId(1);
        astron.setName("Astronomy (ASTRON)");
        astron.setDescription("Description of research and results in modern extragalactic astronomy and cosmology.");
        
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setName("Stephen W. Hawking");
        teacher.setAcademicDegree("Ph.D");
        teacher.setCourse(astron);
        
        Student studentOne = new Student();
        studentOne.setId(1);
        studentOne.setName("Alison Smith");
        Student studentTwo = new Student();
        studentTwo.setId(2);
        studentTwo.setName("Cameron McLean");
        
        List<Student> students = new ArrayList<>();
        students.add(studentOne);
        students.add(studentTwo);
        
        Group group = new Group();
        group.setId(1);
        group.setName("a935");
        group.setStudents(students);
        
        LocalDateTime dateTime = LocalDateTime.of(2018, Month.JANUARY, 10, 9, 00);
        
        lessonOne.setDateTime(dateTime);
        lessonOne.setCourse(astron);
        lessonOne.setTeacher(teacher);
        lessonOne.setAuditorium(auditorium);
        lessonOne.setGroup(group);
        
        List<Lesson> lessons = new ArrayList<>();
        lessons.add(lessonOne);
        university.setShedule(lessons);
        
    }
    
    //- teacher's tests
    @Test
    public void shouldFindTeachersSheduleByDate(){

        Teacher teacherExpected = new Teacher();
        teacherExpected.setId(1);
        teacherExpected.setName("Stephen W. Hawking");
        teacherExpected.setAcademicDegree("Ph.D");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> lessonResult = new ArrayList<>();
        lessonResult = university.getTeachersSheduleByDate(teacherExpected, dateExpected);
        
        LocalDate dateResult = lessonResult.get(0).getDateTime().toLocalDate();
        assertEquals(dateExpected, dateResult);
    }

    @Test
    public void shouldReturnEmptyTeacherListWhenWrongDate(){
        
        Teacher teacherExpected = new Teacher();
        teacherExpected.setName("Stephen W. Hawking");
        teacherExpected.setAcademicDegree("Ph.D");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 11);
        
        List<Lesson> result = new ArrayList<>();
        result = university.getTeachersSheduleByDate(teacherExpected, dateExpected);
        assertEquals(0, result.size());
        
    }

    @Test
    public void shouldReturnEmptyListWhenWrongTeacher(){
        
        Teacher teacherExpected = new Teacher();
        teacherExpected.setId(2);
        teacherExpected.setName("Philip J. Fry");
        teacherExpected.setAcademicDegree("Ph.D");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> result = new ArrayList<>();
        result = university.getTeachersSheduleByDate(teacherExpected, dateExpected);
        assertEquals(0, result.size());
        
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void shouldThrowIllegalArgumentExceptionWhenDateIsNullTeacherCase() { 
        
        Teacher teacherExpected = new Teacher();
        teacherExpected.setName("Stephen W. Hawking");
        teacherExpected.setAcademicDegree("Ph.D");
        
        university.getTeachersSheduleByDate(teacherExpected, null); 
    }

    @Test(expected = IllegalArgumentException.class) 
    public void shouldThrowIllegalArgumentExceptionWhenTeacherIsNull() { 
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        university.getTeachersSheduleByDate(null, dateExpected); 
    }
    
    @Test
    public void shouldFindTeachersSheduleByMonth(){

        Teacher teacherExpected = new Teacher();
        teacherExpected.setId(1);
        teacherExpected.setName("Stephen W. Hawking");
        teacherExpected.setAcademicDegree("Ph.D");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> lessonResult = new ArrayList<>();
        lessonResult = university.getTeachersSheduleByDate(teacherExpected, dateExpected);
        
        LocalDate dateResult = lessonResult.get(0).getDateTime().toLocalDate();
        assertEquals(dateExpected, dateResult);
    }
    
    @Test
    public void shouldReturnEmptyTeacherListWhenWrongMonth(){
        
        Teacher teacherExpected = new Teacher();
        teacherExpected.setId(1);
        teacherExpected.setName("Stephen W. Hawking");
        teacherExpected.setAcademicDegree("Ph.D");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.APRIL, 11);
        
        List<Lesson> result = new ArrayList<>();
        result = university.getTeachersSheduleByDate(teacherExpected, dateExpected);
        assertEquals(0, result.size());
    }
    
    //- student's test
    @Test
    public void shouldFindStudentsSheduleByDate(){

        Student studentExpected = new Student();
        studentExpected.setId(1);
        studentExpected.setName("Alison Smith");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> lessonResult = new ArrayList<>();
        lessonResult = university.getStudentsSheduleByDate(studentExpected, dateExpected);
        
        LocalDate dateResult = lessonResult.get(0).getDateTime().toLocalDate();
        assertEquals(dateExpected, dateResult);
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void shouldThrowIllegalArgumentExceptionWhenStudentIsNull() { 
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        university.getStudentsSheduleByDate(null, dateExpected); 
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void shouldThrowIllegalArgumentExceptionWhenDateIsNullStudentCase() { 
        
        Student studentExpected = new Student();
        studentExpected.setId(1);
        studentExpected.setName("Alison Smith");
        
        university.getStudentsSheduleByDate(studentExpected, null); 
    }
    
    @Test
    public void shouldReturnEmptyStudentListWhenWrongDate(){
        
        Student studentExpected = new Student();
        studentExpected.setName("Alison Smith");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.APRIL, 11);
        
        List<Lesson> result = new ArrayList<>();
        result = university.getStudentsSheduleByDate(studentExpected, dateExpected);
        assertEquals(0, result.size());
        
    }
    
    @Test
    public void shouldReturnEmptyListWhenWrongStudent(){
        
        Student studentExpected = new Student();
        studentExpected.setId(3);
        studentExpected.setName("Deborah Goodwin");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> result = new ArrayList<>();
        result = university.getStudentsSheduleByDate(studentExpected, dateExpected);
        assertEquals(0, result.size());
        
    }
    
    @Test
    public void shouldFindStudentsSheduleByMonth(){

        Student studentExpected = new Student();
        studentExpected.setId(1);
        studentExpected.setName("Alison Smith");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.JANUARY, 10);
        
        List<Lesson> lessonResult = new ArrayList<>();
        lessonResult = university.getStudentsSheduleByDate(studentExpected, dateExpected);
        
        LocalDate dateResult = lessonResult.get(0).getDateTime().toLocalDate();
        assertEquals(dateExpected, dateResult);
    }

    @Test
    public void shouldReturnEmptyStudentListWhenWrongMonth(){
        
        Student studentExpected = new Student();
        studentExpected.setId(1);
        studentExpected.setName("Alison Smith");
        
        LocalDate dateExpected = LocalDate.of(2018, Month.APRIL, 10);
        
        List<Lesson> lessonResult = new ArrayList<>();
        lessonResult = university.getStudentsSheduleByDate(studentExpected, dateExpected);
        
        assertEquals(0, lessonResult.size());
    }
}
