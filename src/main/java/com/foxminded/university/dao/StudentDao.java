package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Group;
import com.foxminded.university.domain.Student;

public class StudentDao {
    
    private static final Logger log = LoggerFactory.getLogger(StudentDao.class);
    
    public Student create(Student student) throws DAOException {
        
        log.trace("Insert new student to database: {}", student);
        String insert = "INSERT INTO students (name) VALUES (?)";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, student.getName());
            log.debug("Query \"{}\" executing", insert);
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                student.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            log.error("Cannot insert student to database. {}", e);
            throw new DAOException("Cannot insert student to database", e);
        }
        log.info("Student created: {}", student);
        return student;
    }
    
    public Student findById(Integer id) throws DAOException {
        
        log.trace("Serching student with id = {}", id);
        String query = "SELECT * FROM students WHERE id = ?";
        Student student = null;
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            log.debug("Query \"{}\" with id = {} executing", query, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            log.error("Cannot find student in database. {}", e);
            throw new DAOException("Cannot find student in database", e);
        }
        log.debug("Student retrieved: {}", student);
        return student;
    }

    public List<Student> findByGroupId(Integer group_id) throws DAOException {
        
        log.trace("Select all students with group_id = {}", group_id);
        String query = "SELECT * FROM students WHERE group_id = ?";
        List<Student> students = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, group_id);
            log.debug("Query \"{}\" with id = {} executing", query, group_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                students.add(student);
            }
        } catch (Exception e) {
            log.error("Cannot find students in database. {}", e);
            throw new DAOException("Cannot find students in database", e);
        }
        log.debug("Students retrieved: {}", students.toString());
        return students;
    }
    
    public List<Student> findAll() throws DAOException {
        
        log.trace("Select all students");
        String query = "SELECT * FROM students";
        List<Student> students = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                students.add(student);
            }
        } catch (Exception e) {
            log.error("Cannot find students in database. {}", e);
            throw new DAOException("Cannot find students in database", e);
        }
        log.debug("Students retrieved: {}", students.toString());
        return students;
    }
    
    public boolean update(Student student) throws DAOException {
        
        log.trace("Updating {} in database", student);
        String query = "UPDATE students SET name = ? WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, student.getName());
            pStatement.setInt(2, student.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Student successfully updated: {}", student);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot update student in database. {}", e);
            throw new DAOException("Cannot update student in database", e);
        }
        log.debug("Student is not updated {}", student);
        return false;
    }
    
    public boolean updateGroupId(Student student, Group group) throws DAOException {
        
        log.trace("Update students group id = {} where student id = {}", group.getId(), student.getId());
        String query = "UPDATE students SET group_id = ? WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, group.getId());
            pStatement.setInt(2, student.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Student successfully updated: {}", student);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot insert group id to database. {}", e);
            throw new DAOException("Cannot add group id in database", e);
        }
        log.debug("Group id = {} added to student with id = {}", group.getId(), student.getId());
        return false;
    }
    
    public boolean delete(Student student) throws DAOException {
        
        log.trace("Deleting {} from database", student);
        String query = "DELETE FROM students WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, student.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Student successfully deleted {}", student);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot delete student from database. {}", e);
            throw new DAOException("Cannot delete student from database", e);
        }
        log.debug("Student is not deleted: {}", student);
        return false;
    }
}
