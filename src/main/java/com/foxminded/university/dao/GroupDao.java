package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Faculty;
import com.foxminded.university.domain.Group;
import com.foxminded.university.domain.Student;

public class GroupDao {
    
    private static final Logger log = LoggerFactory.getLogger(GroupDao.class);
    
    public Group create(Group group) throws DAOException {
        
        log.trace("Insert new group to database: {}", group);
        String insert = "INSERT INTO groups (name) VALUES (?)";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, group.getName());
            log.debug("Query \"{}\" executing", insert);
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                group.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            log.error("Cannot insert group to database. {}", e);
            throw new DAOException("Cannot insert group to database", e);
        }
        log.info("Group created: {}", group);
        return group;
    }
    
    public Group findById(Integer id) throws DAOException {

        log.trace("Serching group with id = {}", id);
        String query = "SELECT * FROM groups WHERE id = ?";
        Group group = null;
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            log.debug("Query \"{}\" with id = {} executing", query, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                group = new Group();
                group.setId(rs.getInt("id"));
                group.setName(rs.getString("name"));
            }
        } catch (Exception e) {
            log.error("Cannot find group in database. {}", e);
            throw new DAOException("Cannot find group in database", e);
        }
        log.debug("Group retrieved: {}", group);
        return group;
    }
    
    public List<Group> findByFacultyId(Integer faculty_id) throws DAOException {
        
        log.trace("Select all groups with faculty_id = {}", faculty_id);
        String query = "SELECT * FROM groups WHERE faculty_id = ?";
        List<Group> groups = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty_id);
            log.debug("Query \"{}\" with id = {} executing", query, faculty_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Group group = new Group();
                group.setId(rs.getInt("id"));
                group.setName(rs.getString("name"));
                groups.add(group);
            }
        } catch (Exception e) {
            log.error("Cannot find groups in database. {}", e);
            throw new DAOException("Cannot find groups in database", e);
        }
        log.debug("Groups retrieved: {}", groups.toString());
        return groups;
    }
    
    public List<Group> findAll() throws DAOException {
        
        log.trace("Select all groups");
        String query = "SELECT * FROM groups";
        List<Group> groups = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Group group = new Group();
                group.setId(rs.getInt("id"));
                group.setName(rs.getString("name"));
                groups.add(group);
            }
        } catch (Exception e) {
            log.error("Cannot find groups in database. {}", e);
            throw new DAOException("Cannot find groups in database", e);
        }
        log.debug("Groups retrieved: {}", groups.toString());
        return groups;
    }
    
    public boolean update(Group group) throws DAOException {
        
        log.trace("Updating {} in database", group);
        String query = "UPDATE groups SET name = ? WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, group.getName());
            pStatement.setInt(2, group.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Group successfully updated: {}", group);
                return true;
            }
        } catch (Exception e) {
            log.error("Cannot update group in database. {}", e);
            throw new DAOException("Cannot update group in database", e);
        }
        log.debug("Group is not updated {}", group);
        return false;
    }
    
    public boolean updateFacultyId(Group group, Faculty faculty) throws DAOException {
        
        log.trace("Update groups faculty id = {} where group id = {}", faculty.getId(), group.getId());
        String query = "UPDATE groups SET faculty_id = ? WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.setInt(2, group.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Group successfully updated: {}", group);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot insert faculty id to database. {}", e);
            throw new DAOException("Cannot add faculty id in database", e);
        }
        log.debug("Faculty id = {} added to group with id = {}", faculty.getId(), group.getId());
        return false;
    }
    
    public boolean delete(Group group) throws DAOException {
        
        log.trace("Deleting {} from database", group);
        String query = "DELETE FROM groups WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, group.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Group successfully deleted {}", group);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot delete group from database. {}", e);
            throw new DAOException("Cannot delete group from database", e);
        }
        log.debug("Group is not deleted: {}", group);
        return false;
    }

    public boolean deleteStudentByGroupId(Group group) throws DAOException {
        
        log.trace("Deleting students from group {} from database", group);
        String query = "DELETE FROM student WHERE group_id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, group.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Students with group id = {} successfully deleted", group.getId());
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot delete students in database. {}", e);
            throw new DAOException("Cannot delete student in database", e);
        }
        log.debug("Students with group id = {} are not deleted", group.getId());
        return false;
    }
}
