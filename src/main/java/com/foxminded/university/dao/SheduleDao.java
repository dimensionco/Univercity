package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.foxminded.university.domain.Lesson;
import com.foxminded.university.domain.Student;
import com.foxminded.university.domain.Teacher;

public class SheduleDao {
    
    public List<Lesson> getTeachersSheduleByDate(Teacher teacher, LocalDate date) throws DAOException{
        
        String query = "SELECT l.id, l.date_time, l.course_id, l.teacher_id, l.auditorium_id, l.group_id " + 
                        "FROM Lessons l " + 
                        "WHERE l.date_time::date = to_date(?, 'YYYY-MM-DD') AND l.teacher_id = ?";
        List<Lesson> lessons = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, date.toString());
            pStatement.setInt(2, teacher.getId());
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setDateTime(rs.getObject("date_time", LocalDateTime.class));
                lesson.setCourse(new CourseDao().findById(rs.getInt("course_id")));
                lesson.setTeacher(new TeacherDao().findById(rs.getInt("teacher_id")));
                lesson.setAuditorium(new AuditoriumDao().findById(rs.getInt("auditorium_id")));
                lesson.setGroup(new GroupDao().findById(rs.getInt("group_id")));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find auditorium", e);
        }
        
        return lessons;
    }
    
    public List<Lesson> getTeachersSheduleByMonth(Teacher teacher, LocalDate date) throws DAOException{
        
        String getShedule = "SELECT l.id, l.date_time, l.course_id, l.teacher_id, l.auditorium_id, l.group_id " + 
                            "FROM Lessons l " + 
                            "WHERE date_part('month', DATE ''?'') = date_part('month', l.date_time) AND " + 
                            "      date_part('year', DATE ''?'') = date_part('year', l.date_time) AND " +
                            "      l.teacher_id = ?;";
        List<Lesson> lessons = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(getShedule)) {
            pStatement.setString(1, date.toString());
            
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//            java.util.Date util_StartDate = format.parse(date.toString());
//            java.sql.Date sql_StartDate = new java.sql.Date( util_StartDate.getTime() );
//            
//            pStatement.setDate(1, sql_StartDate);
            pStatement.setString(2, date.toString());
            pStatement.setInt(3, teacher.getId());
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setDateTime(rs.getObject("date_time", LocalDateTime.class));
                lesson.setCourse(new CourseDao().findById(rs.getInt("course_id")));
                lesson.setTeacher(new TeacherDao().findById(rs.getInt("teacher_id")));
                lesson.setAuditorium(new AuditoriumDao().findById(rs.getInt("auditorium_id")));
                lesson.setGroup(new GroupDao().findById(rs.getInt("group_id")));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find auditorium", e);
        }
        
        return lessons;
    }
    
    public List<Lesson> getStudentsSheduleByDate(Student student, LocalDate date) throws DAOException{
        
        String getGrouptID = "SELECT g.id " + 
                                "FROM Groups g " + 
                                "INNER JOIN Students s ON g.id = s.group_id " + 
                                "WHERE s.id = ?;";
        
        String getShedule = "SELECT l.id, l.date_time, l.course_id, l.teacher_id, l.auditorium_id, l.group_id " + 
                            "FROM Lessons l " + 
                            "WHERE l.date_time::date = to_date(?, 'YYYY-MM-DD') AND l.group_id = ?";
        List<Lesson> lessons = new ArrayList<>();
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatGetStudentId = connection.prepareStatement(getGrouptID);
                PreparedStatement pStatGetShedule = connection.prepareStatement(getShedule);) {
            
            pStatGetStudentId.setInt(1, student.getId());
            ResultSet rs1 = pStatGetStudentId.executeQuery();
            Integer groupId = null;
            if (rs1.next()) {
                groupId = rs1.getInt("id");
            } else {
                return lessons;
            }
            pStatGetShedule.setString(1, date.toString());
            pStatGetShedule.setInt(2, groupId);
            ResultSet rs = pStatGetShedule.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setDateTime(rs.getObject("date_time", LocalDateTime.class));
                lesson.setCourse(new CourseDao().findById(rs.getInt("course_id")));
                lesson.setTeacher(new TeacherDao().findById(rs.getInt("teacher_id")));
                lesson.setAuditorium(new AuditoriumDao().findById(rs.getInt("auditorium_id")));
                lesson.setGroup(new GroupDao().findById(rs.getInt("group_id")));
                lessons.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find auditorium", e);
        }
        
        return lessons;
    }
}
