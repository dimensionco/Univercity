package com.foxminded.university.dao;

@SuppressWarnings("serial")
public class DAOException extends RuntimeException{
    
    public DAOException() {
        super();
    }

    public DAOException(String message, Throwable cause) {
        super();
    }
    public DAOException(String message) {
        super();
    }
}
