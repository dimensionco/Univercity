package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Auditorium;
import com.foxminded.university.domain.Faculty;
import com.foxminded.university.domain.Lesson;
import com.foxminded.university.domain.University;

public class UniversityDao {
    
    private static final Logger log = LoggerFactory.getLogger(GroupDao.class);
    
    public University create(University university) throws DAOException {
        
        String insert = "INSERT INTO university (name) VALUES ?";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, university.getName());
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                university.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add university", e);
        }
        return university;
    }
    
    public Faculty addFacultiesUniversityId(Faculty faculty, University university) throws DAOException {
        
        String insert = "INSERT INTO faculties (university_id) VALUES ?";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, university.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add university id", e);
        }
        return faculty;
    }

    public Lesson addLessonsUniversityId(Lesson lesson, University university) throws DAOException {
        
        String insert = "INSERT INTO lessons (university_id) VALUES ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, university.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add university id", e);
        }
        return lesson;
    }

    public Auditorium addAuditoriumsUniversityId(Auditorium auditorium, University university) throws DAOException {
        
        String insert = "INSERT INTO groups (university_id) VALUES ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, university.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add university id", e);
        }
        return auditorium;
    }
    
    public University findById(Integer id) throws DAOException {
        
        String query = "SELECT * FROM university WHERE id = ?";
        University university = null;
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                university = new University();
                university.setId(rs.getInt("id"));
                university.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find university", e);
        }
        return university;
    }
    
    public List<University> findAll() throws DAOException {
        
        log.trace("Select university");
        String query = "SELECT * FROM university";
        List<University> universities = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                University university = new University();
                university.setId(rs.getInt("id"));
                university.setName(rs.getString("name"));
                universities.add(university);
            }
        } catch (Exception e) {
            log.error("Cannot find universities in database. {}", e);
            throw new DAOException("Cannot find universities in database", e);
        }
        log.debug("Universities retrieved: {}", universities.toString());
        return universities;
    }
    
    public boolean update(University university) throws DAOException {
        
        String query = "UPDATE university SET name = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, university.getName());
            pStatement.setInt(4, university.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update university", e);
        }
        return false;
    }
    
    public boolean updateFacultiesUniversityId(Faculty faculty, University university) throws DAOException {
        
        String query = "UPDATE faculty SET university_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            pStatement.setInt(2, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update university id", e);
        }
        return false;
    }
    
    public boolean updateLessonsUniversityId(Lesson lesson, University university) throws DAOException {
        
        String query = "UPDATE lessons SET university_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            pStatement.setInt(2, lesson.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update university id", e);
        }
        return false;
    }
    
    public boolean updateAuditoriumsUniversityId(Auditorium auditorium, University university) throws DAOException {
        
        String query = "UPDATE groups SET university_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            pStatement.setInt(2, auditorium.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update university id", e);
        }
        return false;
    }
    
    public boolean delete(University university) throws DAOException {
        
        String query = "DELETE FROM university WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete university", e);
        }
        return false;
    }
    
    public boolean deleteFacultyByUniversityId(University university) throws DAOException {
        
        String query = "DELETE FROM faculties WHERE university_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete faculty", e);
        }
        return false;
    }
    
    public boolean deleteLessonByUniversityId(University university) throws DAOException {
        
        String query = "DELETE FROM lessons WHERE university_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete lesson", e);
        }
        return false;
    }
    
    public boolean deleteAuditoriumByUniversityId(University university) throws DAOException {
        
        String query = "DELETE FROM auditorium WHERE university_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete auditorium", e);
        }
        return false;
    }
}
