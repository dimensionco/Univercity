package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Course;

public class CourseDao {
    
    private static final Logger log = LoggerFactory.getLogger(StudentDao.class);
    
    public Course create(Course course) throws DAOException {
        
        String insert = "INSERT INTO course (name, description) VALUES (?, ?)";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, course.getName());
            pStatement.setString(2, course.getDescription());
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                course.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add course", e);
        }
        return course;
    }

    public Course findById(Integer id) throws DAOException {
        
        String query = "SELECT * FROM courses WHERE id = ?";
        Course course = null;
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                course = new Course();
                course.setId(rs.getInt("id"));
                course.setName(rs.getString("name"));
                course.setDescription(rs.getString("description"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find course", e);
        }
        return course;
    }
    
    public List<Course> findByFacultyId(Integer faculty_id) throws DAOException {
        
        log.trace("Select all courses with faculty_id = {}", faculty_id);
        String query = "SELECT * FROM courses WHERE faculty_id = ?";
        List<Course> courses = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty_id);
            log.debug("Query \"{}\" with id = {} executing", query, faculty_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("id"));
                course.setName(rs.getString("name"));
                course.setDescription(rs.getString("description"));
                courses.add(course);
            }
        } catch (Exception e) {
            log.error("Cannot find courses in database. {}", e);
            throw new DAOException("Cannot find courses in database", e);
        }
        log.debug("Courses retrieved: {}", courses.toString());
        return courses;
    }
    
    public List<Course> findAll() throws DAOException {
        
        log.trace("Select all courses");
        String query = "SELECT * FROM courses";
        List<Course> courses = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("id"));
                course.setName(rs.getString("name"));
                course.setDescription(rs.getString("description"));
                courses.add(course);
            }
        } catch (Exception e) {
            log.error("Cannot find course in database. {}", e);
            throw new DAOException("Cannot find courses in database", e);
        }
        log.debug("Courses retrieved: {}", courses.toString());
        return courses;
    }
    
    public boolean update(Course course) throws DAOException {
        
        String query = "UPDATE course SET name = ?, description = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, course.getName());
            pStatement.setString(2, course.getDescription());
            pStatement.setInt(3, course.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update course", e);
        }
        return false;
    }
    
    public boolean delete(Course course) throws DAOException {
        
        String query = "DELETE FROM courses WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, course.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete course", e);
        }
        return false;
    }
}
