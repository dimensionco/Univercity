package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Teacher;

public class TeacherDao {
    
    private static final Logger log = LoggerFactory.getLogger(StudentDao.class);
    
    public Teacher create(Teacher teacher) throws DAOException {
        
        String insert = "INSERT INTO teachers (name, academic_degree, course_id) VALUES (?, ?, ?)";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, teacher.getName());
            pStatement.setString(2, teacher.getAcademicDegree());
            pStatement.setInt(3, teacher.getCourse().getId());
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                teacher.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add teacher", e);
        }
        return teacher;
    }
    
    public Teacher findById(Integer id) throws DAOException {
        
        String query = "SELECT * FROM teachers WHERE id = ?";
        Teacher teacher = null;
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                teacher = new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setName(rs.getString("name"));
                teacher.setAcademicDegree(rs.getString("academic_degree"));
                teacher.setCourse(new CourseDao().findById(rs.getInt("course_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find teacher", e);
        }
        return teacher;
    }
    
    public List<Teacher> findByFacultyId(Integer faculty_id) throws DAOException {
        
        log.trace("Select all teachers with faculty_id = {}", faculty_id);
        String query = "SELECT * FROM teachers WHERE faculty_id = ?";
        List<Teacher> teachers = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty_id);
            log.debug("Query \"{}\" with id = {} executing", query, faculty_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Teacher teacher = new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setName(rs.getString("name"));
                teacher.setAcademicDegree(rs.getString("academic_degree"));
                teachers.add(teacher);
            }
        } catch (Exception e) {
            log.error("Cannot find teachers in database. {}", e);
            throw new DAOException("Cannot find teachers in database", e);
        }
        log.debug("Teachers retrieved: {}", teachers.toString());
        return teachers;
    }

    public List<Teacher> findAll() throws DAOException {
        
        log.trace("Select all teachers");
        String query = "SELECT * FROM teachers";
        List<Teacher> teachers = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Teacher teacher = new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setName(rs.getString("name"));
                teacher.setAcademicDegree(rs.getString("academic_degree"));
                teachers.add(teacher);
            }
        } catch (Exception e) {
            log.error("Cannot find students in database. {}", e);
            throw new DAOException("Cannot find students in database", e);
        }
        log.debug("Students retrieved: {}", teachers.toString());
        return teachers;
    }
    
    public boolean update(Teacher teacher) throws DAOException {
        
        String query = "UPDATE teachers SET name = ?, academic_degree = ?, course_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, teacher.getName());
            pStatement.setString(2, teacher.getAcademicDegree());
            pStatement.setInt(3, teacher.getCourse().getId());
            pStatement.setInt(4, teacher.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update teacher", e);
        }
        return false;
    }
    
    public boolean delete(Teacher teacher) throws DAOException {
        
        String query = "DELETE FROM teachers WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, teacher.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete teacher", e);
        }
        return false;
    }
}
