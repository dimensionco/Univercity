package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Lesson;

public class LessonDao {
    
    private static final Logger log = LoggerFactory.getLogger(LessonDao.class);
    
    public Lesson create(Lesson lesson) throws DAOException {
        
        log.trace("Insert new lesson to database: {}", lesson);
        String insert = "INSERT INTO lesson (date_time, course_id, teacher_id, auditorium_id, group_id) VALUES (?, ?, ?, ?, ?)";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setObject(1, lesson.getDateTime());
            pStatement.setInt(2, lesson.getCourse().getId());
            pStatement.setInt(3, lesson.getTeacher().getId());
            pStatement.setInt(4, lesson.getAuditorium().getId());
            pStatement.setInt(5, lesson.getGroup().getId());
            log.debug("Query \"{}\" executing", insert);
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                lesson.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            log.error("Cannot insert lesson to database. {}", e);
            throw new DAOException("Cannot insert lesson to database.", e);
        }
        log.info("Lesson created: {}", lesson);
        return lesson;
    }
    
    public Lesson findById(Integer id) throws DAOException {
        
        log.trace("Serching lesson with id = {}", id);
        String query = "SELECT * FROM lessons WHERE id = ?";
        Lesson lesson = null;
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            log.debug("Query \"{}\" with id = {} executing", query, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setDateTime(rs.getObject("date_time", LocalDateTime.class));
                lesson.setCourse(new CourseDao().findById(rs.getInt("course_id")));
                lesson.setTeacher(new TeacherDao().findById(rs.getInt("teacher_id")));
                lesson.setAuditorium(new AuditoriumDao().findById(rs.getInt("auditorium_id")));
                lesson.setGroup(new GroupDao().findById(rs.getInt("group_id")));
            }
        } catch (SQLException e) {
            log.error("Cannot find lesson in database. {}", e);
            throw new DAOException("Cannot find lesson in database.", e);
        }
        log.debug("Lesson retrieved: {}", lesson);
        return lesson;
    }
    
    public List<Lesson> findAll() throws DAOException {
        
        log.trace("Select all lessons");
        String query = "SELECT * FROM lessons";
        List<Lesson> lessons = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setDateTime(rs.getObject("date_time", LocalDateTime.class));
                lessons.add(lesson);
            }
        } catch (Exception e) {
            log.error("Cannot find lessons in database. {}", e);
            throw new DAOException("Cannot find lessons in database", e);
        }
        log.debug("Lessons retrieved: {}", lessons.toString());
        return lessons;
    }
    
    public boolean update(Lesson lesson) throws DAOException {
        
        log.trace("Updating {} in lesson", lesson);
        String query = "UPDATE lessons SET date_time = ?, course_id = ?, teacher_id = ?, auditorium_id = ?, group_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setObject(1, lesson.getDateTime());
            pStatement.setInt(2, lesson.getCourse().getId());
            pStatement.setInt(3, lesson.getTeacher().getId());
            pStatement.setInt(4, lesson.getAuditorium().getId());
            pStatement.setInt(5, lesson.getGroup().getId());
            pStatement.setInt(6, lesson.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("lesson successfully updated: {}", lesson);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot update lesson in database. {}", e);
            throw new DAOException("Cannot update lesson in database.", e);
        }
        log.debug("Student is not updated {}", lesson);
        return false;
    }
    
    public boolean delete(Lesson lesson) throws DAOException {
        
        log.trace("Deleting {} from database", lesson);
        String query = "DELETE FROM lessons WHERE id = ?;";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, lesson.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Lesson successfully deleted {}", lesson);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot delete lesson from database. {}", e);
            throw new DAOException("Cannot delete lesson from database.", e);
        }
        log.debug("Lesson is not deleted: {}", lesson);
        return false;
    }
}
