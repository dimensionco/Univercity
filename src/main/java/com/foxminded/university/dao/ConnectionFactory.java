package com.foxminded.university.dao;

import java.io.InputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionFactory {

    private static final Logger log = LoggerFactory.getLogger(ConnectionFactory.class);
    private static Properties properties;

    public static Connection getConnection() throws SQLException {
        
        if (properties == null) {
            log.trace("Loading properties");
            loadProperties();
            loadDriver();
            log.trace("Properties loaded");
        }
        return DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"), properties.getProperty("password"));
    }
    
    private static void loadProperties() {

        log.trace("Searching db properties file");
        properties = new Properties();
//        String file = "/config/jdbc_postgresql.conf";
        String file = "/config/jdbc_mysql.conf";

        try (InputStream is = ConnectionFactory.class.getResourceAsStream(file)) {
            log.trace("Db properties file found. Loading properties from file");
            properties.load(is);
        } catch (IOException e) {
            log.error("Cannot load properties.", e);
            throw new DAOException("Cannot find db properties file", e);
        }
        log.debug("Properties loaded");
    }
    
    private static void loadDriver() {
        try {
            log.trace("Loadind driver");
            Class.forName(properties.getProperty("driver"));
        } catch (ClassNotFoundException e) {
            log.error("Cannot load driver. Exception {%s}", e);
            throw new DAOException("Cannot load driver", e);
        }
        log.info("Driver loaded");
    }
}
