package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Auditorium;

public class AuditoriumDao {
    
    private static final Logger log = LoggerFactory.getLogger(AuditoriumDao.class);

    public Auditorium create(Auditorium auditorium) throws DAOException {
        
        log.trace("Insert new auditorium to database: {}", auditorium);
        String insert = "INSERT INTO auditorium (name) VALUES ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, auditorium.getNumber());
            log.debug("Query \"{}\" executing", insert);
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                auditorium.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            log.error("Cannot insert auditorium to database. {%s}", e);
            throw new DAOException("Cannot insert auditorium to database.", e);
        }
        log.info("Auditorium created: {}", auditorium);
        return auditorium;
    }

    public Auditorium findById(Integer id) throws DAOException {
        
        log.trace("Serching auditorium with id = {}", id);
        String query = "SELECT * FROM auditoriums WHERE id = ?";
        Auditorium auditorium = null;
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            log.debug("Query \"{}\" with id = {} executing", query, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                auditorium = new Auditorium();
                auditorium.setId(rs.getInt("id"));
                auditorium.setNumber(rs.getString("name"));
            }
        } catch (SQLException e) {
            log.error("Cannot find auditorium in database. {}", e);
            throw new DAOException("Cannot find auditorium in database.", e);
        }
        log.debug("Auditorium retrieved: {}", auditorium);
        return auditorium;
    }
    
    public List<Auditorium> findByUniversityId(Integer university_id) throws DAOException {
        
        log.trace("Select all auditoriums with university_id = {}", university_id);
        String query = "SELECT * FROM auditoriums WHERE university_id = ?";
        List<Auditorium> auditoriums = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university_id);
            log.debug("Query \"{}\" with id = {} executing", query, university_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Auditorium auditorium = new Auditorium();
                auditorium.setId(rs.getInt("id"));
                auditorium.setNumber(rs.getString("name"));
                auditoriums.add(auditorium);
            }
        } catch (Exception e) {
            log.error("Cannot find auditoriums in database. {}", e);
            throw new DAOException("Cannot find auditoriums in database", e);
        }
        log.debug("Auditoriums retrieved: {}", auditoriums.toString());
        return auditoriums;
    }
    
    public List<Auditorium> findAll() throws DAOException {
        
        log.trace("Select all auditoriums");
        String query = "SELECT * FROM auditoriums";
        List<Auditorium> auditoriums = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Auditorium auditorium = new Auditorium();
                auditorium.setId(rs.getInt("id"));
                auditorium.setNumber(rs.getString("name"));
                auditoriums.add(auditorium);
            }
        } catch (Exception e) {
            log.error("Cannot find auditoriums in database. \"{%s}\"", e);
            throw new DAOException("Cannot find auditoriums", e);
        }
        log.debug("Auditoriums retrieved: {}", auditoriums.toString());
        return auditoriums;
    }

    public boolean update(Auditorium auditorium) throws DAOException {
        
        log.trace("Updating {} in database", auditorium);
        String query = "UPDATE auditorium SET number = ? WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, auditorium.getNumber());
            pStatement.setInt(2, auditorium.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Auditoium successfully updated: {}", auditorium);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot update auditorium in database. {}", e);
            throw new DAOException("Cannot update auditorium in database.", e);
        }
        log.debug("Auditorium is not updated {}", auditorium);
        return false;
    }

    public boolean delete(Auditorium auditorium) throws DAOException {
        
        log.trace("Deleting {} from database", auditorium);
        String query = "DELETE FROM auditorium WHERE id = ?";
        
        log.trace("Open connection. Create prepared statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, auditorium.getId());
            log.debug("Query \"{}\" executing", query);
            int result = pStatement.executeUpdate();
            if (result == 1) {
                log.debug("Auditorium successfully deleted {}", auditorium);
                return true;
            }
        } catch (SQLException e) {
            log.error("Cannot delete auditorium from database. {}", e);
            throw new DAOException("Cannot delete auditorium from database.", e);
        }
        log.debug("Auditorium is not deleted: {}", auditorium);
        return false;
    }
}
