package com.foxminded.university.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foxminded.university.domain.Course;
import com.foxminded.university.domain.Faculty;
import com.foxminded.university.domain.Group;
import com.foxminded.university.domain.Teacher;

public class FacultyDao {
    
    private static final Logger log = LoggerFactory.getLogger(GroupDao.class);
    
    public Faculty create(Faculty faculty) throws DAOException {
        
        String insert = "INSERT INTO faculty (name) VALUES ?";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pStatement.setString(1, faculty.getName());
            pStatement.executeUpdate();
            ResultSet rs = pStatement.getGeneratedKeys();
            if (rs.next()) {
                faculty.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add faculty", e);
        }
        return faculty;
    }
    
    public Course addCorsesFacultyId(Course course, Faculty faculty) throws DAOException {
        
        String insert = "INSERT INTO courses (faculty_id) VALUES ?";
                
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add faculty id", e);
        }
        return course;
    }

    public Teacher addTeachersFacultyId(Teacher teacher, Faculty faculty) throws DAOException {
        
        String insert = "INSERT INTO teachers (faculty_id) VALUES ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add faculty id", e);
        }
        return teacher;
    }

    public Group addGroupsFacultyId(Group group, Faculty faculty) throws DAOException {
        
        String insert = "INSERT INTO groups (faculty_id) VALUES ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(insert)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot add faculty id", e);
        }
        return group;
    }
    
    public List<Faculty> findByUniversityId(Integer university_id) throws DAOException {
        
        log.trace("Select all faculties with university_id = {}", university_id);
        String query = "SELECT * FROM faculties WHERE university_id = ?";
        List<Faculty> faculties = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, university_id);
            log.debug("Query \"{}\" with id = {} executing", query, university_id);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                Faculty faculty = new Faculty();
                faculty.setId(rs.getInt("id"));
                faculty.setName(rs.getString("name"));
                faculties.add(faculty);
            }
        } catch (Exception e) {
            log.error("Cannot find faculties in database. {}", e);
            throw new DAOException("Cannot find faculties in database", e);
        }
        log.debug("Faculties retrieved: {}", faculties.toString());
        return faculties;
    }
    
    public Faculty findById(Integer id) throws DAOException {
        
        String query = "SELECT * FROM faculties WHERE id = ?";
        Faculty faculty = null;
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, id);
            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                faculty = new Faculty();
                faculty.setId(rs.getInt("id"));
                faculty.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot find faculty", e);
        }
        return faculty;
    }
    
    public List<Faculty> findAll() throws DAOException {
        
        log.trace("Select all faculties");
        String query = "SELECT * FROM faculties";
        List<Faculty> faculties = new ArrayList<>();

        log.trace("Open connection. Create statement");
        try (Connection connection = ConnectionFactory.getConnection();
                Statement statement = connection.createStatement()) {
            log.debug("Query \"{}\"executing", query);
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Faculty faculty = new Faculty();
                faculty.setId(rs.getInt("id"));
                faculty.setName(rs.getString("name"));
                faculties.add(faculty);
            }
        } catch (Exception e) {
            log.error("Cannot find faculties in database. {}", e);
            throw new DAOException("Cannot find faculties in database", e);
        }
        log.debug("Faculties retrieved: {}", faculties.toString());
        return faculties;
    }
    
    public boolean update(Faculty faculty) throws DAOException {
        
        String query = "UPDATE faculty SET name = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setString(1, faculty.getName());
            pStatement.setInt(4, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update faculty", e);
        }
        return false;
    }
    
    public boolean updateCoursesFacultyId(Course course, Faculty faculty) throws DAOException {
        
        String query = "UPDATE courses SET faculty_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.setInt(2, course.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update faculty id", e);
        }
        return false;
    }
    
    public boolean updateTeachersFacultyId(Teacher teacher, Faculty faculty) throws DAOException {
        
        String query = "UPDATE teachers SET faculty_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.setInt(2, teacher.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update faculty id", e);
        }
        return false;
    }
    
    public boolean updateGroupsFacultyId(Group group, Faculty faculty) throws DAOException {
        
        String query = "UPDATE groups SET faculty_id = ? WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            pStatement.setInt(2, group.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot update faculty id", e);
        }
        return false;
    }
    
    public boolean delete(Faculty faculty) throws DAOException {
        
        String query = "DELETE FROM faculties WHERE id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete faculty", e);
        }
        return false;
    }
    
    public boolean deleteCourseByFacultyId(Faculty faculty) throws DAOException {
        
        String query = "DELETE FROM courses WHERE faculty_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete course", e);
        }
        return false;
    }
    
    public boolean deleteTeacherByFacultyId(Faculty faculty) throws DAOException {
        
        String query = "DELETE FROM teachers WHERE faculty_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete teacher", e);
        }
        return false;
    }
    
    public boolean deleteGroupByFacultyId(Faculty faculty) throws DAOException {
        
        String query = "DELETE FROM groups WHERE faculty_id = ?";
        
        try (Connection connection = ConnectionFactory.getConnection();
                PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, faculty.getId());
            int result = pStatement.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete group", e);
        }
        return false;
    }
}
