package com.foxminded.university.domain;

import java.io.Serializable;

public class Auditorium implements Serializable{
    
    private Integer id;
    private String number;
    
    public Integer getId() {
        return id;
    }
    
    public String getNumber() {
        return number;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Auditorium)) {
            return false;
        }
        Auditorium other = (Auditorium) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Auditorium [id=" + id + ", number=" + number + "]";
    }
}
