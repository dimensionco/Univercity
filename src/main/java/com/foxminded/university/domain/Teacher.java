package com.foxminded.university.domain;

import java.io.Serializable;

public class Teacher implements Serializable{
    
    private Integer id;
    private String name;
    private String academicDegree;
    private transient Course course;

    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getAcademicDegree() {
        return academicDegree;
    }
    
    public Course getCourse() {
        return course;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }
    
    public void setCourse(Course course) {
        this.course = course;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Teacher)) {
            return false;
        }
        Teacher other = (Teacher) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Teacher [id=" + id + ", name=" + name + ", academicDegree=" + academicDegree + "]";
    }
}
