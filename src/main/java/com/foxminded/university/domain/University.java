package com.foxminded.university.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class University implements Serializable{
    
    private Integer id;
    private String name;
    private transient List<Faculty> faculties;
    private transient List<Lesson> shedule;
    private transient List<Auditorium> auditoriums;
    
    public List<Lesson> getTeachersSheduleByDate(Teacher teacher, LocalDate date) {
        
        if (date == null) {
            throw new IllegalArgumentException("Date cannot be Null");
        }
        
        if (teacher == null) {
            throw new IllegalArgumentException("Teacher cannot be Null");
        }
        
        List<Lesson> lessons = new ArrayList<>();
        for (Lesson lesson : shedule) {
            LocalDate lessonDate = lesson.getDateTime().toLocalDate();
            if (lessonDate.isEqual(date) && (lesson.getTeacher().equals(teacher))) {
                lessons.add(lesson);
            }
        }

        return lessons;
    }
    
    public List<Lesson> getTeachersSheduleByMonth(Teacher teacher, LocalDate date) {
        
        if (date == null) {
            throw new IllegalArgumentException("Date cannot be Null");
        }
        
        if (teacher == null) {
            throw new IllegalArgumentException("Teacher cannot be Null");
        }
        
        List<Lesson> lessons = new ArrayList<>();
        for (Lesson lesson : shedule) {
            LocalDate lessonMonth = lesson.getDateTime().toLocalDate();
            if ((lessonMonth.getYear() == date.getYear()) && (lessonMonth.getMonth().equals(date.getMonth()))) {
                if (lesson.getTeacher().equals(teacher)) {
                    lessons.add(lesson);
                }
            }
        }
        
        return lessons;
    }
    
    public List<Lesson> getStudentsSheduleByDate(Student student, LocalDate date) {
        
        if (date == null) {
            throw new IllegalArgumentException("Date cannot be Null");
        }
        
        if (student == null) {
            throw new IllegalArgumentException("Student cannot be Null");
        }
        
        List<Lesson> lessons = new ArrayList<>();
        Group studentGroup = new Group();
        for (Lesson lesson : shedule) {
            for (Student studentExpected : lesson.getGroup().getStudents()) {
                if (studentExpected.equals(student)) {
                    studentGroup = lesson.getGroup();
                    break;
                }
            }
        }
        for (Lesson lesson : shedule) {
            LocalDate lessonDate = lesson.getDateTime().toLocalDate();
            if (lessonDate.isEqual(date) && (lesson.getGroup().equals(studentGroup))) {
                lessons.add(lesson);
            }
        }

        return lessons;
    }

    public List<Lesson> getStudentsSheduleByMonth(Student student, LocalDate date) {
        
        if (date == null) {
            throw new IllegalArgumentException("Date cannot be Null");
        }
        
        if (student == null) {
            throw new IllegalArgumentException("Student cannot be Null");
        }
        
        List<Lesson> lessons = new ArrayList<>();
        Group studentGroup = new Group();
        for (Lesson lesson : shedule) {
            for (Student studentExpected : lesson.getGroup().getStudents()) {
                if (studentExpected.equals(student)) {
                    studentGroup = lesson.getGroup();
                    break;
                }
            }
        }
        for (Lesson lesson : shedule) {
            LocalDate lessonMonth = lesson.getDateTime().toLocalDate();
            if ((lessonMonth.getYear() == date.getYear()) && (lessonMonth.getMonth().equals(date.getMonth()))) {
                if (lesson.getGroup().equals(studentGroup)) {
                    lessons.add(lesson);
                }
            }
        }
        
        return lessons;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Faculty> getFaculties() {
        return faculties;
    }
    
    public List<Lesson> getShedule() {
        return shedule;
    }
    
    public List<Auditorium> getAuditoriums() {
        return auditoriums;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setFaculties(List<Faculty> faculties) {
        this.faculties = faculties;
    }
    
    public void setShedule(List<Lesson> shedules) {
        this.shedule = shedules;
    }

    public void setAuditoriums(List<Auditorium> auditoriums) {
        this.auditoriums = auditoriums;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof University)) {
            return false;
        }
        University other = (University) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "University [id=" + id + ", name=" + name + "]";
    }
}
