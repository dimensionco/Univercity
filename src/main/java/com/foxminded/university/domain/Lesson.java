package com.foxminded.university.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Lesson implements Serializable{
    
    private Integer id;
    private LocalDateTime dateTime;
    private Course course;
    private Teacher teacher;
    private Auditorium auditorium;
    private Group group;

    public Integer getId() {
        return id;
    }
    
    public LocalDateTime getDateTime() {
        return dateTime;
    }
    
    public Course getCourse() {
        return course;
    }
    
    public Teacher getTeacher() {
        return teacher;
    }
    
    public Auditorium getAuditorium() {
        return auditorium;
    }
    
    public Group getGroup() {
        return group;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    
    public void setCourse(Course course) {
        this.course = course;
    }
    
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
    
    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }
    
    public void setGroup(Group group) {
        this.group = group;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Lesson)) {
            return false;
        }
        Lesson other = (Lesson) obj;
        if (auditorium == null) {
            if (other.auditorium != null) {
                return false;
            }
        } else if (!auditorium.equals(other.auditorium)) {
            return false;
        }
        if (course == null) {
            if (other.course != null) {
                return false;
            }
        } else if (!course.equals(other.course)) {
            return false;
        }
        if (dateTime == null) {
            if (other.dateTime != null) {
                return false;
            }
        } else if (!dateTime.equals(other.dateTime)) {
            return false;
        }
        if (group == null) {
            if (other.group != null) {
                return false;
            }
        } else if (!group.equals(other.group)) {
            return false;
        }
        if (teacher == null) {
            if (other.teacher != null) {
                return false;
            }
        } else if (!teacher.equals(other.teacher)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((auditorium == null) ? 0 : auditorium.hashCode());
        result = prime * result + ((course == null) ? 0 : course.hashCode());
        result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
        result = prime * result + ((group == null) ? 0 : group.hashCode());
        result = prime * result + ((teacher == null) ? 0 : teacher.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Lesson [dateTime=" + dateTime + ", course=" + course + ", teacher=" + teacher + ", auditorium="
                + auditorium + ", group=" + group + "]";
    }
}
