package com.foxminded.university.domain;

import java.io.Serializable;
import java.util.List;

public class Faculty implements Serializable{
    
    private Integer id;
    private String name;
    private transient List<Course> courses;
    private transient List<Teacher> teachers;
    private transient List<Group> groups;
    
    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public List<Course> getCourses() {
        return courses;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }
    
    public List<Group> getGroups() {
        return groups;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
    
    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Faculty)) {
            return false;
        }
        Faculty other = (Faculty) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Faculty [id=" + id + ", name=" + name + "]";
    }
}
