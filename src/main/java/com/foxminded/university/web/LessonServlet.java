package com.foxminded.university.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.LessonDao;
import com.foxminded.university.domain.Auditorium;
import com.foxminded.university.domain.Course;
import com.foxminded.university.domain.Group;
import com.foxminded.university.domain.Lesson;
import com.foxminded.university.domain.Teacher;

@WebServlet("/lesson")
public class LessonServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    int id = Integer.parseInt(request.getParameter("id"));
        LessonDao lessonDao = new LessonDao();
        Lesson lesson = lessonDao.findById(id);
        request.setAttribute("lesson", lesson);
        Course course = lesson.getCourse();
        request.setAttribute("course", course);
        Teacher teacher = lesson.getTeacher();
        request.setAttribute("teacher", teacher);
        Auditorium auditorium = lesson.getAuditorium();
        request.setAttribute("auditorium", auditorium);
        Group group = lesson.getGroup();
        request.setAttribute("group", group);
        request.getRequestDispatcher("lesson.jsp").forward(request,response);
	}

}
