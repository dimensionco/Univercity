package com.foxminded.university.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.AuditoriumDao;

@WebServlet("/auditorium")
public class AuditoriumServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        AuditoriumDao auditoriumDao = new AuditoriumDao();
        request.setAttribute("auditorium", auditoriumDao.findById(id));
        request.getRequestDispatcher("auditorium.jsp").forward(request,response);
    }
}
