package com.foxminded.university.web.group;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.GroupDao;

@WebServlet("/group/groupeditform")
public class GroupEditFormServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private GroupDao groupDao = new GroupDao();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("group", groupDao.findById(id));
        request.getRequestDispatcher("groupeditform.jsp").forward(request, response);
    }
}
