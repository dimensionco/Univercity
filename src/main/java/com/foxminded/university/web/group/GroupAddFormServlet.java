package com.foxminded.university.web.group;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.FacultyDao;

@WebServlet("/group/groupaddform")
public class GroupAddFormServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private FacultyDao facultyDao = new FacultyDao();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("faculties", facultyDao.findAll());
        request.getRequestDispatcher("groupaddform.jsp").forward(request,response);
    }
}
