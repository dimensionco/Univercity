package com.foxminded.university.web.group;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.GroupDao;
import com.foxminded.university.domain.Faculty;
import com.foxminded.university.domain.Group;

@WebServlet("/groups")
public class GroupsServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private GroupDao groupDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	initDao();
        request.setAttribute("groups", groupDao.findAll());
	    request.getRequestDispatcher("groups.jsp").forward(request,response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("faculty"));
        Faculty faculty = new Faculty();
        faculty.setId(id);
        Group group = new Group();
        group.setName(request.getParameter("name"));
        initDao();
        groupDao.create(group);
        groupDao.updateFacultyId(group, faculty);
        response.sendRedirect("faculty?id=" + id);
    }
    
    private void initDao() {
        groupDao = new GroupDao();
    }
}
