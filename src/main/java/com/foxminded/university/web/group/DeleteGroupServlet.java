package com.foxminded.university.web.group;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.GroupDao;
import com.foxminded.university.domain.Group;

@WebServlet("/deletegroup")
public class DeleteGroupServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private GroupDao groupDao = new GroupDao();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Group group = new Group();
        group.setId(id);
        groupDao.delete(group);
        response.sendRedirect("groups");
    }
}
