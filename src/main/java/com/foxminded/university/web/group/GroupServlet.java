package com.foxminded.university.web.group;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.GroupDao;
import com.foxminded.university.dao.StudentDao;
import com.foxminded.university.service.GroupService;

@WebServlet("/group")
public class GroupServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private GroupDao groupDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        StudentDao studentDao = new StudentDao();
        initDao();
        request.setAttribute("group", groupDao.findById(id));
        request.setAttribute("students", studentDao.findByGroupId(id));
        request.getRequestDispatcher("group.jsp").forward(request,response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GroupService groupService = new GroupService();
        initDao();
        groupDao.update(groupService.updateGroup(request, response));
        response.sendRedirect("groups");
    }
    
    private void initDao() {
        groupDao = new GroupDao();
    }
}
