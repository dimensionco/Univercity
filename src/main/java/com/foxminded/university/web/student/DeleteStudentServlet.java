package com.foxminded.university.web.student;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.StudentDao;
import com.foxminded.university.domain.Student;

@WebServlet("/deletestudent")
public class DeleteStudentServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private StudentDao studentDao = new StudentDao();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Student student = new Student();
        student.setId(id);
        studentDao.delete(student);
        response.sendRedirect("students");
    }
}
