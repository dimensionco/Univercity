package com.foxminded.university.web.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.StudentDao;
import com.foxminded.university.service.StudentService;

@WebServlet("/student")
public class StudentServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private StudentDao studentDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        initDao();
        request.setAttribute("student", studentDao.findById(id));
        request.getRequestDispatcher("student.jsp").forward(request,response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StudentService studentService = new StudentService();
        initDao();
        studentDao.update(studentService.updateStudent(request, response));
        response.sendRedirect("students");
    }
    
    private void initDao() {
        studentDao = new StudentDao();
    }
}
