package com.foxminded.university.web.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.StudentDao;
import com.foxminded.university.domain.Group;
import com.foxminded.university.domain.Student;

@WebServlet("/students")
public class StudentsServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private StudentDao studentDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initDao();
        request.setAttribute("students", studentDao.findAll());
        request.getRequestDispatcher("students.jsp").forward(request,response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("group"));
        initDao();
        Group group = new Group();
        group.setId(id);
        Student student = new Student();
        student.setName(request.getParameter("name"));
        student = studentDao.create(student);
        studentDao.updateGroupId(student, group);
        response.sendRedirect("group?id=" + id);
    }
    
    private void initDao() {
        studentDao = new StudentDao();
    }
}
