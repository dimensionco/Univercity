package com.foxminded.university.web.student;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.StudentDao;

@WebServlet("/student/studenteditform")
public class StudentEditFormServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private StudentDao studentDao = new StudentDao();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("student", studentDao.findById(id));
        request.getRequestDispatcher("studenteditform.jsp").forward(request,response);
    }
}
