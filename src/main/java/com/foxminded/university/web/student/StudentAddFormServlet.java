package com.foxminded.university.web.student;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.GroupDao;

@WebServlet("/student/studentaddform")
public class StudentAddFormServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    private GroupDao groupDao = new GroupDao();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("groups", groupDao.findAll());
        request.getRequestDispatcher("studentaddform.jsp").forward(request,response);
    }
}
