package com.foxminded.university.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.LessonDao;
import com.foxminded.university.domain.Lesson;

@WebServlet("/lessons")
public class LessonsServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    LessonDao lessonDao = new LessonDao();
	    List<Lesson> lessons = lessonDao.findAll();
        request.setAttribute("lessons", lessons);
        request.getRequestDispatcher("lessons.jsp").forward(request,response);
	}

}
