package com.foxminded.university.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.AuditoriumDao;
import com.foxminded.university.dao.FacultyDao;
import com.foxminded.university.dao.UniversityDao;

@WebServlet("/university")
public class UniversityServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    int id = Integer.parseInt(request.getParameter("id"));
	    UniversityDao universityDao = new UniversityDao();
	    FacultyDao facultyDao = new FacultyDao();
	    AuditoriumDao auditoriumDao = new AuditoriumDao();
	    request.setAttribute("university", universityDao.findById(id));
	    request.setAttribute("faculties", facultyDao.findByUniversityId(id));
	    request.setAttribute("auditoriums", auditoriumDao.findByUniversityId(id));
	    request.getRequestDispatcher("university.jsp").forward(request,response);
	}
}
