package com.foxminded.university.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.TeacherDao;

@WebServlet("/teachers")
public class TeachersServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    TeacherDao teacherDao = new TeacherDao();
        request.setAttribute("teachers", teacherDao.findAll());
        request.getRequestDispatcher("teachers.jsp").forward(request,response);
	}
}
