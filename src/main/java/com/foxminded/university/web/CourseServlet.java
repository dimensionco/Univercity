package com.foxminded.university.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.CourseDao;

@WebServlet("/course")
public class CourseServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        CourseDao courseDao = new CourseDao();
        request.setAttribute("course", courseDao.findById(id));
        request.getRequestDispatcher("course.jsp").forward(request,response);
    }
}
