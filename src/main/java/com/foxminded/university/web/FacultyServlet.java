package com.foxminded.university.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.FacultyDao;
import com.foxminded.university.dao.GroupDao;
import com.foxminded.university.dao.TeacherDao;
import com.foxminded.university.dao.CourseDao;

@WebServlet("/faculty")
public class FacultyServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        FacultyDao facultyDao = new FacultyDao();
        CourseDao courseDao = new CourseDao();
        TeacherDao teacherDao = new TeacherDao();
        GroupDao groupDao = new GroupDao();
        request.setAttribute("faculty", facultyDao.findById(id));
        request.setAttribute("courses", courseDao.findByFacultyId(id));
        request.setAttribute("teachers", teacherDao.findByFacultyId(id));
        request.setAttribute("groups", groupDao.findByFacultyId(id));
        request.getRequestDispatcher("faculty.jsp").forward(request,response);
    }

}
