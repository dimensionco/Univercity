package com.foxminded.university.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.AuditoriumDao;

@WebServlet("/auditoriums")
public class AuditoriumsServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuditoriumDao auditoriumDao = new AuditoriumDao();
        request.setAttribute("auditoriums", auditoriumDao.findAll());
        request.getRequestDispatcher("auditoriums.jsp").forward(request,response);
    }
}
