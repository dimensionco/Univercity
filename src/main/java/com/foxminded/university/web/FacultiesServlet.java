package com.foxminded.university.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.FacultyDao;

@WebServlet("/faculties")
public class FacultiesServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FacultyDao facultyDao = new FacultyDao();
        request.setAttribute("faculties", facultyDao.findAll());
        request.getRequestDispatcher("faculties.jsp").forward(request,response);
    }

}
