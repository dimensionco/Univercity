package com.foxminded.university.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.dao.TeacherDao;
import com.foxminded.university.domain.Course;
import com.foxminded.university.domain.Teacher;

@WebServlet("/teacher")
public class TeacherServlet extends HttpServlet {
	
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        TeacherDao teacherDao = new TeacherDao();
        Teacher teacher = teacherDao.findById(id);
        request.setAttribute("teacher", teacher);
        Course course = teacher.getCourse();
        request.setAttribute("course", course);
        request.getRequestDispatcher("teacher.jsp").forward(request,response);
    }
}
