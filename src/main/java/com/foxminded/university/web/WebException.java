package com.foxminded.university.web;

@SuppressWarnings("serial")
public class WebException extends RuntimeException{
    
    public WebException() {
        super();
    }

    public WebException(String message, Throwable cause) {
        super();
    }
    public WebException(String message) {
        super();
    }
}
