package com.foxminded.university.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.domain.Student;

public class StudentService {
    
    private Student student = new Student();
    
    public Student updateStudent(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        student.setId(id);
        student.setName(name);
        return student;
    }
}
