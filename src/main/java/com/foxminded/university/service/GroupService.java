package com.foxminded.university.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.university.domain.Faculty;
import com.foxminded.university.domain.Group;

public class GroupService {
    
    private Group group= new Group();

//    public Group createGroup(HttpServletRequest request, HttpServletResponse response) {
//        int id = Integer.parseInt(request.getParameter("faculty"));
//        Faculty faculty = new Faculty();
//        faculty.setId(id);
//        Group group = new Group();
//        group.setName(request.getParameter("name"));
//        return group;
//    }
    public Group updateGroup(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        group.setId(id);
        group.setName(name);
        return group;
    }
}
