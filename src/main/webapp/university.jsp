<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>University</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    <h2>University:</h2>
    <b>${university.name}</b>
    <h3>Faculties:</h3>
    <ul> 
        <c:forEach items="${faculties}" var="faculty">
            <li>
                <a href="${pageContext.request.contextPath}/faculty?id=${faculty.id}">${faculty.name}</a>
            </li>
        </c:forEach>
    </ul>
    <h3>Schedule:</h3>
    <ul> 
        <li>
            <a href="${pageContext.request.contextPath}/lessons">Lessons schedule</a>
        </li>
    </ul>
    <h3>Auditoriums:</h3>
    <ul> 
        <c:forEach items="${auditoriums}" var="auditorium">
            <li>
                <a href="${pageContext.request.contextPath}/auditorium?id=${auditorium.id}">${auditorium.number}</a>
            </li>
        </c:forEach>
    </ul>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>