<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Group</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    <h2>Group:</h2>
    <b>${group.name}</b>
    <ul>
        <c:forEach items="${students}" var="student">
            <li>
                <a href="${pageContext.request.contextPath}/student?id=${student.id}">${student.name}</a>
            </li>
        </c:forEach>
    </ul>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>