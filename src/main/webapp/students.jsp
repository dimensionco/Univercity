<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    <h2>Students:</h2>
    <table width=400px>
        <tr>
            <td width=10px align="right"></td>
            <td width=240px align=left><a class="add" href="${pageContext.request.contextPath}/student/studentaddform">Add new student</a></td>
            <td width=20px></td>
            <td width=20px></td>
        </tr>
        <c:forEach items="${students}" var="student">
            <tr>
                <td>&bull;</td>
                <td><a href="${pageContext.request.contextPath}/student?id=${student.id}">${student.name}</a></td>
                <td><a class="edit" href="${pageContext.request.contextPath}/student/studenteditform?id=${student.id}">Edit</a></td>
                <td>
                    <form action="${pageContext.request.contextPath}/deletestudent" method="post">
                        <input class="del" type="submit" name="delete_student" value="Delete" />
                        <input type="hidden" name="id" value="${student.id}" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
