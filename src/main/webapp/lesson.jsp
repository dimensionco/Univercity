<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Lesson</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    <h2>Lesson:</h2>
    <ul>
        <li>${lesson.dateTime}</li>
    </ul>
    <h3>Course:</h3>
    <ul> 
        <li>
            <a href="${pageContext.request.contextPath}/course?id=${course.id}">${course.name}</a>
        </li>
    </ul>
    <h3>Teacher:</h3>
    <ul> 
        <li>
            <a href="${pageContext.request.contextPath}/teacher?id=${teacher.id}">${teacher.name}&nbsp;${teacher.academicDegree}</a>
        </li>
    </ul>
    <h3>Auditorium:</h3>
    <ul> 
        <li>
            <a href="${pageContext.request.contextPath}/auditorium?id=${auditorium.id}">${auditorium.number}</a>
        </li>
    </ul>
    <h3>Group:</h3>
    <ul> 
        <li>
            <a href="${pageContext.request.contextPath}/group?id=${group.id}">${group.name}</a>
        </li>
    </ul>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>