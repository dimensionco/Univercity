<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div calss="menu">

    <a href="${pageContext.request.contextPath}/">Home</a>
    |
    <a href="${pageContext.request.contextPath}/auditoriums">Auditoriums</a>
    |
    <a href="${pageContext.request.contextPath}/courses">Courses</a>
    |
    <a href="${pageContext.request.contextPath}/faculties">Faculties</a>
    |
    <a href="${pageContext.request.contextPath}/groups">Groups</a>
    |
    <a href="${pageContext.request.contextPath}/lessons">Lessons</a>
    |
    <a href="${pageContext.request.contextPath}/students">Students</a>
    |
    <a href="${pageContext.request.contextPath}/teachers">Teachers</a>
    |
    <a href="${pageContext.request.contextPath}/universities">University</a>

</div>
