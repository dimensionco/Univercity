<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Groups</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    <h2>Groups:</h2>
    <table width=400px>
        <tr>
            <td width=10px align="right"></td>
            <td width=240px align=left><a class="add" href="${pageContext.request.contextPath}/group/groupaddform">Add new group</a></td>
            <td width=20px></td>
            <td width=20px></td>
        </tr>
        <c:forEach items="${groups}" var="group">
            <tr>
                <td>&bull;</td>
                <td><a href="${pageContext.request.contextPath}/group?id=${group.id}">${group.name}</a></td>
                <td><a class="edit" href="${pageContext.request.contextPath}/group/groupeditform?id=${group.id}">Edit</a></td>
                <td>
                    <form action="${pageContext.request.contextPath}/deletegroup" method="post">
                        <input class="del" type="submit" name="delete_group" value="Delete" />
                        <input type="hidden" name="id" value="${group.id}" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
