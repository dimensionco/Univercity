<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Add new group</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <jsp:include page="../header.jsp"></jsp:include>
    <jsp:include page="../menu.jsp"></jsp:include>
    <h2>Group:</h2>
    <form action="../groups" method="post">
        <table>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <td>Faculty:</td>
                <td><select name="faculty">  
                        <c:forEach items="${faculties}" var="faculty">
                            <option value="${faculty.id}">${faculty.name}</option>
                        </c:forEach>  
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Add group"/>
                <input type=button value="Cancel" onCLick="history.back()"></td>
            </tr>
        </table>
    </form>
    <jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>